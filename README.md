# Libre Mare logos

A collection of logos and promotional material for Libre Mare

## License

&copy; 2018 [Libre Mare](https://libremare.gr).

Licensed under the [Creative Commons Attribution-ShareAlike 3.0 License](LICENSE).
